VERSION = (0, 6, 9, 2)
VERSION_STR = ".".join(map(str, VERSION))
VERSION_SHORT = ".".join(map(str, [VERSION[0], VERSION[1]]))
